/**
 * @file MyLog.h
 * Описание пространства имен MyLog.
 * Содержит описание макросов работы с журналом, перечисления и класс myLog
 */

#ifndef MYLOG_H_
#define MYLOG_H_

#include <QObject>
#include <QString>
#include <QVariant>
#include <QMutex>

#include <settingscmdline/settingscmdline.h>

/**
 * @mainpage
 * @section Описание
 * Библиотека простого журналирования в консоль и в файл для программ на Qt
 *
 * Отправка сообщений происходит через макросы и глобальный объект.
 * Поддерживается смена уровня "на лету".
 */

//! Имя файла журнала по умолчанию
#define LOG_DEFAULT_FILENAME	"res/log/Log.%DATE%.log"

//! Форматы вывода по умолчанию @{

//! Формат даты в журнале по умолчанию
#define LOG_DEFAULT_DATE_FORMAT "dd.MM.yyyy"
//! Формат времени в журнале по умолчанию
#define LOG_DEFAULT_TIME_FORMAT	"h:mm:ss.z"
//! @}

//! @brief Сообщение с Фатальной ошибкой
//! @param msg Текст сообщения, отправляемого в журнал
#define LOG_FATAL(msg)		MyLog::LOG->addMessage(msg, MyLog::LOG_FATAL, __FUNCTION__)

//! @brief Сообщение с ошибкой
//! @param msg Текст сообщения, отправляемого в журнал
#define LOG_ERROR(msg)		MyLog::LOG->addMessage(msg, MyLog::LOG_ERROR, __FUNCTION__)

//! @brief Сообщение с предупреждением
//! @param msg Текст сообщения, отправляемого в журнал
#define LOG_WARNING(msg)	MyLog::LOG->addMessage(msg, MyLog::LOG_WARNING, __FUNCTION__)

//! @brief Сообщение с информацией
//! @param msg Текст сообщения, отправляемого в журнал
#define LOG_INFO(msg)		MyLog::LOG->addMessage(msg, MyLog::LOG_INFO, __FUNCTION__)

//! @brief Отладочное сообщение
//! @param msg Текст сообщения, отправляемого в журнал
#define LOG_DEBUG(msg)		MyLog::LOG->addMessage(msg, MyLog::LOG_DEBUG, __FUNCTION__)

//! @brief Сообщение с выбраным уровнем
//! @param msg Текст сообщения, отправляемого в журнал
//! @param type Уровень отправляемого значения @link enum enLogLevel @endlink
#define LOG_type(msg, type)	MyLog::LOG->addMessage(msg, type, __FUNCTION__)

class QFile;
class QTextStream;
class QLabel;

/**
 * Пространство имен журнала
 */
namespace MyLog
{
/**
 * @brief Перечисление уровня сообщения
 */
enum enLogLevel
{
    //! Фатальная ошибка
    LOG_FATAL = 0,

    //! Ошибка
    LOG_ERROR = 1,

    //! Предупреждение
    LOG_WARNING = 2,

    //! Информация
    LOG_INFO = 3,

    //! Сообщение для отладки
    LOG_DEBUG = 4
};

/**
 * @brief Класс журнала
 * Содержит методы для отправки записей в журнал
 */
class myLog: public QObject
{
    Q_OBJECT
public:

    /**
     * @param parent "Родитель"
     */
    myLog(QObject* parent = NULL);
    ~myLog();

    /**
     * @brief Установить имя файла в который будет записываться журнал
     * @param name строка, содержащая полный или относительный путь к новому файлу журнала
     *  Если name содержит "%1", то он будет заменен на дату начата журнала в формате ISO
     */
    void setLogFileName(const QString& name);

    /**
     * @brief Установить новый формат Даты в выводе журнала
     * @param DateFormat Строка формата даты. Значение по умолчанию берется из настроек, ключ: Log/LogDateFormat
     * @param TimeFormat Строка формата времени. Значение по умолчанию берется из настроек, ключ: Log/LogTimeFormat
     */
    void setLogDateTimeFormat(const QString& DateFormat =
            SettingsCmdLine::settings->value("Log/LogDateFormat", LOG_DEFAULT_DATE_FORMAT).toString(),
                              const QString& TimeFormat =
            SettingsCmdLine::settings->value("Log/LogTimeFormat", LOG_DEFAULT_TIME_FORMAT).toString());

    /**
     * @brief Узнать текущий базовый уровень журналирования
     * @return текущий уровень журналирования (одно из  @link enLogLevel @endlink)
     */
    enum enLogLevel getLogLevel(void) const;

    /**
     * @brief Запросить перевод строки перед следующим выводом журнала (действует только на вывод в терминал)
     */
    void newLineRequest();

public slots:
    /**
     * @brief Добавить сообщения в журнал
     * @param text текст сообщения
     * @param Level Уровень сообщения (одно из @link enLogLevel @endlink)
     * @param additionalText дополнительный текст, будет выведен в
     *          квадратных скобках перед сообщением и только на уровне @link LOG_DEBUG @endlink
     */
    void addMessage(const QString& text, enLogLevel Level = LOG_INFO,
                    const char* additionalText = NULL);

    /**
     * @brief Установить базовый уровень журнала.
     * Все сообщения, уровень которых больше будут отброшены
     * @param logLevel Новый уровень журналирования
     */
    void setLogLevel(enLogLevel logLevel =
            (enLogLevel)SettingsCmdLine::settings->valueIfExists("Log/Level", LOG_INFO).toUInt());

private:
    bool newlinereq;
    bool noFile;

    QString LogDateFormat;
    QString LogTimeFormat;

    enLogLevel CurrentLogLevel;
    mutable QMutex OperationInProgressMutex;

    QFile* Logfile;
    QTextStream* textstream;

signals:
    /**
     * @brief Сигнал, генерируемый при отправке сообщения в журнал из любого места программы.
     * Содержит строку с текстом отправленым в журнал. Используется для дублирования вывода
     * журнала в любое мето пользовательского интерфейса.
     */
    void NewText(const QString&);
};

/**
 * @brief Глобальный указатель на обьект журнала.
 * Нужен для работы макросов журналирования
 * <br>@link LOG_FATAL(msg) @endlink
 * <br>@link LOG_ERROR(msg) @endlink
 * <br>@link LOG_WARNING(msg) @endlink
 * <br>@link LOG_INFO(msg) @endlink
 * <br>@link LOG_DEBUG(msg) @endlink
 * <br>@link LOG_type(msg, type) @endlink
 */
extern myLog* LOG;
}

#endif /* LOG_H_ */
