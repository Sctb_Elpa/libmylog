/*
 * Log.cpp
 *
 *  Created on: 29.06.2012
 *      Author: tolyan
 */

#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <iostream>

#include "mylog.h"

MyLog::myLog* MyLog::LOG;

using namespace MyLog;

myLog::myLog(QObject* parent) :
    QObject(parent)
{
    Logfile = new QFile(this);
    textstream = NULL;
    noFile = SettingsCmdLine::settings->value("Log/NoFile", false).toBool();
    if (!noFile)
        setLogFileName(trUtf8(LOG_DEFAULT_FILENAME));
    setLogLevel();
    setLogDateTimeFormat();
    addMessage(trUtf8("Log started."), LOG_INFO);
    newlinereq = false;
    LOG = this;
}

myLog::~myLog()
{
    if (textstream != NULL)
        delete textstream;
}

void myLog::addMessage(const QString & text, enLogLevel Level,
                       const char* additionalText)
{
    if (Level <= getLogLevel())
    { //игнорируем, все, что больше уровня логирования
        OperationInProgressMutex.lock();
        emit NewText(text);
        QString result;
        QDateTime currentdateTime = QDateTime::currentDateTime();
        switch (Level)
        {
        case LOG_DEBUG:
            result.append(trUtf8("[DEBUG]"));
            break;
        case LOG_INFO:
            result.append(trUtf8("[INFO]"));
            break;
        case LOG_WARNING:
            result.append(trUtf8("[WARNING]"));
            break;
        case LOG_ERROR:
            result.append(trUtf8("[ERROR]"));
            break;
        case LOG_FATAL:
            result.append(trUtf8("[FATAL]"));
            break;
        default:
            result.append(trUtf8("[DEBUG]"));
            break;
        }
        if ((additionalText != NULL) && (CurrentLogLevel == LOG_DEBUG))
            result.append(trUtf8("[%1]\t").arg(additionalText));
        else
            result.append('\t');
        result.append(text);
        if (newlinereq)
        {
            std::cout << '\n';
            newlinereq = false;
        }

        std::cout << result.toLatin1().data() << std::endl;

        if (!noFile)
        {
            result.push_front(
                        trUtf8("[%1][%2]").arg(
                            currentdateTime.date().toString(LogDateFormat)).arg(
                            currentdateTime.time().toString(LogTimeFormat)));
            (*textstream) << result << endl;
        }
        OperationInProgressMutex.unlock();
    }
}

void myLog::setLogFileName(const QString & name)
{
    noFile = false;
    OperationInProgressMutex.lock();
    if (textstream != NULL)
        delete textstream;
    if (Logfile->isOpen())
        Logfile->close();
    QString _name = name;
    _name = _name.replace("%DATE%", QDateTime::currentDateTime().toString(Qt::ISODate));
    _name.replace(QChar(':'), QChar('_'), Qt::CaseInsensitive);
    Logfile->setFileName(_name);
    Logfile->open(QIODevice::WriteOnly | QIODevice::Text);
    textstream = new QTextStream(Logfile);
    OperationInProgressMutex.unlock();
}

void myLog::setLogLevel(enLogLevel logLevel)
{
    OperationInProgressMutex.lock();
    CurrentLogLevel = logLevel;
    OperationInProgressMutex.unlock();
}

enLogLevel myLog::getLogLevel(void) const
{
    enLogLevel res;
    OperationInProgressMutex.lock();
    res = CurrentLogLevel;
    OperationInProgressMutex.unlock();
    return res;
}

void myLog::newLineRequest()
{
    newlinereq = true;
}

void myLog::setLogDateTimeFormat(const QString& DateFormat,
                                 const QString& TimeFormat)
{
    OperationInProgressMutex.lock();
    LogDateFormat = DateFormat;
    LogTimeFormat = TimeFormat;
    OperationInProgressMutex.unlock();
}
